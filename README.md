This project provides a CMake build configuration for [**iniparser**](https://github.com/ndevilla/iniparser).

It aims at providing

- a [bitbake recipe](yocto/iniparser_git.bb) to integrate **iniparser** in yocto images
- a `CMakeLists.txt` and a package config file for your local development environment
- automated tests using Gitlab CI

# TODO

- a standard build and install for easier packaging of **iniparser** in Linux distributions

Please have a look at the [bitbake recipe](yocto/iniparser_git.bb) on how to use
this project to package **iniparser**.

# Getting started

Clone this project:

```shell
git clone https://gitlab.com/iniparser/iniparser-meta.git
```

Change into this project, create a build directory and change into it:

```shell
cd iniparser-meta
mkdir build
cd build
```

## Build and install

This project uses [ExternalProject_Add](https://cmake.org/cmake/help/latest/module/ExternalProject.html).

After configuring the project with `cmake ..` executing `make` will:

- clone
- build and
- install **iniparser**

As the install step is executed on `make`, make sure that you have write access to the installation destination directories. Unless you want to install with superuser power, you should provide a [CMAKE_INSTALL_PREFIX](https://cmake.org/cmake/help/latest/variable/CMAKE_INSTALL_PREFIX.html):

```shell
cmake -DCMAKE_INSTALL_PREFIX=../../install ..
make
```

```
[...]
-- Installing: /home/lars/install/lib64/libiniparser.a
-- Installing: /home/lars/install/include/iniparser/iniparser.h
-- Installing: /home/lars/install/include/iniparser/dictionary.h
-- Installing: /home/lars/install/lib64/cmake/iniparser/iniparser-staticTargets.cmake
-- Installing: /home/lars/install/lib64/cmake/iniparser/iniparser-staticTargets-noconfig.cmake
-- Installing: /home/lars/install/lib64/cmake/iniparser/iniparser-config-version.cmake
-- Installing: /home/lars/install/lib64/cmake/iniparser/iniparser-config.cmake
[...]
```

You know that [BUILD_SHARED_LIBS](https://cmake.org/cmake/help/latest/variable/BUILD_SHARED_LIBS.html) will `cmake` make build and install shared libraries.

```shell
cmake -DCMAKE_INSTALL_PREFIX=../../install -DBUILD_SHARED_LIBS=ON ..
make
```

```
[...]
-- Installing: /home/lars/install/lib64/libiniparser.so.4.2.0
-- Installing: /home/lars/install/lib64/libiniparser.so.4
-- Installing: /home/lars/install/lib64/libiniparser.so
-- Installing: /home/lars/install/include/iniparser/iniparser.h
-- Installing: /home/lars/install/include/iniparser/dictionary.h
-- Installing: /home/lars/install/lib64/cmake/iniparser/iniparser-sharedTargets.cmake
-- Installing: /home/lars/install/lib64/cmake/iniparser/iniparser-sharedTargets-noconfig.cmake
-- Installing: /home/lars/install/lib64/cmake/iniparser/iniparser-config-version.cmake
-- Installing: /home/lars/install/lib64/cmake/iniparser/iniparser-config.cmake
[...]
```

For easier packaging with bitbake, `iniparser-meta` provides you with the custom option `BUILD_STATIC_WITH_SHARED_LIBS`.

```shell
cmake -DCMAKE_INSTALL_PREFIX=../../install -DBUILD_SHARED_LIBS=ON -DBUILD_STATIC_WITH_SHARED_LIBS=ON ..
make
```

```
[...]
-- Installing: /home/lars/install/lib64/libiniparser.so.4.2.0
-- Up-to-date: /home/lars/install/lib64/libiniparser.so.4
-- Up-to-date: /home/lars/install/lib64/libiniparser.so
-- Installing: /home/lars/install/include/iniparser/iniparser.h
-- Installing: /home/lars/install/include/iniparser/dictionary.h
-- Installing: /home/lars/install/lib64/cmake/iniparser/iniparser-sharedTargets.cmake
-- Installing: /home/lars/install/lib64/cmake/iniparser/iniparser-sharedTargets-noconfig.cmake
-- Installing: /home/lars/install/lib64/libiniparser.a
-- Up-to-date: /home/lars/install/include/iniparser/iniparser.h
-- Up-to-date: /home/lars/install/include/iniparser/dictionary.h
-- Installing: /home/lars/install/lib64/cmake/iniparser/iniparser-staticTargets.cmake
-- Installing: /home/lars/install/lib64/cmake/iniparser/iniparser-staticTargets-noconfig.cmake
-- Installing: /home/lars/install/lib64/cmake/iniparser/iniparser-config-version.cmake
-- Installing: /home/lars/install/lib64/cmake/iniparser/iniparser-config.cmake
[...]
```

## Running tests

To build and install the tests enable the custom CMake option `BUILD_TESTS`:

```shell
cmake -DCMAKE_INSTALL_PREFIX=../../install -DBUILD_TESTS=ON ..
make
```
```
[...]
-- Installing: /home/lars/install/lib64/libiniparser.a
-- Installing: /home/lars/install/include/iniparser/iniparser.h
-- Installing: /home/lars/install/include/iniparser/dictionary.h
-- Installing: /home/lars/install/lib64/cmake/iniparser/iniparser-staticTargets.cmake
-- Installing: /home/lars/install/lib64/cmake/iniparser/iniparser-staticTargets-noconfig.cmake
-- Installing: /home/lars/install/lib64/cmake/iniparser/iniparser-config-version.cmake
-- Installing: /home/lars/install/lib64/cmake/iniparser/iniparser-config.cmake
-- Installing: /home/lars/install/bin/testrun
-- Up-to-date: /home/lars/install/bin/ressources
-- Up-to-date: /home/lars/install/bin/ressources/bad_ini
-- Installing: /home/lars/install/bin/ressources/bad_ini/twisted-ofval.ini
-- Installing: /home/lars/install/bin/ressources/bad_ini/twisted-ofkey.ini
-- Installing: /home/lars/install/bin/ressources/bad_ini/twisted-errors.ini
-- Installing: /home/lars/install/bin/ressources/bad_ini/ends_well.ini
-- Up-to-date: /home/lars/install/bin/ressources/good_ini
-- Installing: /home/lars/install/bin/ressources/good_ini/empty.ini
-- Installing: /home/lars/install/bin/ressources/good_ini/spaced2.ini
-- Installing: /home/lars/install/bin/ressources/good_ini/spaced.ini
-- Installing: /home/lars/install/bin/ressources/good_ini/twisted.ini
[...]
```

Please note that `testrun` expects to find its ressources at `${PWD}/ressources`. So in order to execute it, you have to change directory:

```shell
cd ../../install/bin
./testrun
```
```
iniparser: cannot open /you/shall/not/path
iniparser: input line too long in ressources/bad_ini/twisted-ofval.ini (14)
iniparser: input line too long in ressources/bad_ini/twisted-ofkey.ini (14)
iniparser: syntax error in ressources/bad_ini/twisted-errors.ini (5):
-> hello
iniparser: syntax error in ressources/bad_ini/twisted-errors.ini (6):
-> world
iniparser: syntax error in ressources/bad_ini/twisted-errors.ini (8):
-> hello world
iniparser: syntax error in ressources/bad_ini/twisted-errors.ini (9):
-> a + b ;
iniparser: syntax error in ressources/bad_ini/ends_well.ini (5):
-> error is here
iniparser: cannot open /path/to/nowhere.ini
......................

OK (22 tests)

```


## Contributing to iniparser

Per default `iniparser-meta` will clone, build install **iniparser** within its `build` directory:

```
.
├── build
│   ├── CMakeFiles
│   └── iniparser-prefix
│       ├── src
│       │   ├── iniparser
│       │   │   ├── doc
│       │   │   ├── example
│       │   │   ├── html
│       │   │   ├── src
│       │   │   └── test
│       │   ├── iniparser-build
│       │   │   └── CMakeFiles
│       │   └── iniparser-stamp
│       └── tmp
├── cmake
└── yocto
```

It will also *overwrite any changes* to **iniparser** on `make`.

To change this behaviour `iniparser-meta` provides the custom cmake argument `INIPARSER_SOURCE_DIR`.

```shell
cmake -DCMAKE_INSTALL_PREFIX=../../install -DINIPARSER_SOURCE_DIR=../../iniparser ..
make
```

If the directory passed by `INIPARSER_SOURCE_DIR` does not exist, `iniparser-meta` will clone **iniparser** into it and turn it into a CMake project by copying these files there:

- `CMakeLists.txt`
- `config.cmake.in`

And use this source directory to build from.

If the directory passed by `INIPARSER_SOURCE_DIR` does exist, `iniparser-meta` will only copy the files into it but leaves the rest of the sources untouched.

Now you can change into `INIPARSER_SOURCE_DIR` and use it as CMake project in case you want to contribute to **iniparser** itself:

```shell
cd ../../iniparser
mkdir build
cd build/
cmake -DCMAKE_INSTALL_PREFIX=../../install -DBUILD_SHARED_LIBS=ON -DBUILD_STATIC_WITH_SHARED_LIBS=ON ..
make
```
As expected from a regular CMake project `make` won't install, instead use:
```shell
make install
```
```
[...]
-- Installing: /home/lars/install/lib64/libiniparser.so.4.2.0
-- Up-to-date: /home/lars/install/lib64/libiniparser.so.4
-- Up-to-date: /home/lars/install/lib64/libiniparser.so
-- Up-to-date: /home/lars/install/include/iniparser/iniparser.h
-- Up-to-date: /home/lars/install/include/iniparser/dictionary.h
-- Installing: /home/lars/install/lib64/cmake/iniparser/iniparser-sharedTargets.cmake
-- Installing: /home/lars/install/lib64/cmake/iniparser/iniparser-sharedTargets-noconfig.cmake
-- Installing: /home/lars/install/lib64/libiniparser.a
-- Up-to-date: /home/lars/install/include/iniparser/iniparser.h
-- Up-to-date: /home/lars/install/include/iniparser/dictionary.h
-- Installing: /home/lars/install/lib64/cmake/iniparser/iniparser-staticTargets.cmake
-- Installing: /home/lars/install/lib64/cmake/iniparser/iniparser-staticTargets-noconfig.cmake
-- Installing: /home/lars/install/lib64/cmake/iniparser/iniparser-config-version.cmake
-- Installing: /home/lars/install/lib64/cmake/iniparser/iniparser-config.cmake
[...]
```

# Gitlab CI

The Gitlab CI is triggered on each push to [iniparser](https://github.com/ndevilla/iniparser) `master` branch.

By default a pipline will checkout [iniparser](https://github.com/ndevilla/iniparser) `master` branch.
In order to run the pipline for a specific branch you may [run the pipeline manually](https://gitlab.com/iniparser/iniparser-meta/-/pipelines/new) and pass the name of the branch using the `GIT_TAG` variable.

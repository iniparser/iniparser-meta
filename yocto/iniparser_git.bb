inherit cmake

SUMMARY = "INI file parser implemented in C"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://../iniparser/LICENSE;md5=8474d3b745f77e203f1fc82fb0bb7678"
SRCBRANCH = "master"
SRCREV_FORMAT = "iniparser"
SRCREV_iniparser = "${AUTOREV}"
SRCREV_meta = "${AUTOREV}"
SRCREV = "${SRCREV_iniparser}"
PV_append = "+git${SRCPV}"

SRC_URI = "git://github.com/ndevilla/iniparser.git;protocol=https;branch=${SRCBRANCH};name=iniparser;destsuffix=git/iniparser \
           git://gitlab.com/iniparser/iniparser-meta.git;protocol=https;branch=main;name=meta;destsuffix=git/meta"
S = "${WORKDIR}/git/meta"

EXTRA_OECMAKE = " \
	-DBUILD_SHARED_LIBS=ON \
	-DBUILD_STATIC_WITH_SHARED_LIBS=ON \
        -DINIPARSER_SOURCE_DIR=../../iniparser \
        -DCMAKE_INSTALL_PREFIX=install_prefix/ \
	"

FILES_${PN}-dev += "${includedir}/iniparser/*"
FILES_${PN}-dev += "${libdir}/cmake/iniparser/iniparser-config*.cmake"
FILES_${PN}-dev += "${libdir}/cmake/iniparser/iniparser-sharedTargets*.cmake"
FILES_${PN}-staticdev += "${libdir}/cmake/iniparser/iniparser-staticTargets*.cmake"

do_install () {
        install --directory ${D}${prefix}
        cp --no-preserve=ownership --recursive install_prefix/* ${D}/${prefix}
}
